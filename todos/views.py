from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem
# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
    }
    return render(request, 'todos/list.html', context)

def todo_list_detail(request, id):
    tasks = get_object_or_404(TodoList, id=id)
    context = {
        'tasks': tasks
    }
    return render(request, 'todos/detail.html', context)

def redirect_detail(request):
    if request.method == "POST":
        form = ModelForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("detail_url", id=model_instance.id)
    else:
        form = ModelForm()

    context = {
        'form': form
    }
    return render(request, 'model_names/create.html', context)
